﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour
{
    public GameObject destroyedParticles;
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "playerBullet")
        {
            Destroy(gameObject);
        }

        if (col.gameObject.tag == "enemyBullet")
        {
            Instantiate(destroyedParticles, this.transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
